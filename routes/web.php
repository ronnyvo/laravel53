<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/route/basic', function (){
   return 'Very basic routing';
});

Route::get('/route/controller', 'HomeController@showBasicRoute')->name('basic-route');

Route::get('/route/params/{name}', 'HomeController@showParamsRoute')->name('params-route');

Route::get('/route/params-opt/{name?}', 'HomeController@showParamsOptRoute')->name('params-opt-route');

Route::group(['prefix' => 'admin'], function () {
    Route::get('dashboard', function ()    {
        return 'This is admin dashboard page';
    });

    Route::get('users', function ()    {
        return 'This is admin users page';
    });
});
Auth::routes();

Route::get('/home', 'HomeController@index');

//Demo Request & Response
Route::get('/request', 'DemoController@showRequestPage');
Route::post('/request', 'DemoController@getRequest')->name('get-request');

Route::get('/api/users', 'DemoController@apiGetUsers');
Route::get('/redirect', 'DemoController@redirectAction');

//Demo Middle
Route::get('/showAge', 'DemoController@showAgePage');

Route::any('/secret-content', function () {
    return 'Only mature can view this secret content.';
})->middleware('check-age');

//Demo Views & Templates
Route::get('views/simple', function (){
    $name = 'NhanVo';
    return view('demo.viewsimple', ['name' => $name]);
});

//
Route::get('api-auth', function (){
    return view('demo.apiauth');
});

//Mailable
Route::get('mail', function (){

    $user = \App\User::first();

    Mail::to('ronny.vo.7@gmail.com')->send(new \App\Mail\WelcomeMail($user));
});