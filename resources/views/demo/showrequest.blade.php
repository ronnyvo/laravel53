@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form method="post" action="{{ route('get-request') }}">
                    <input type="text" name="name">
                    <input type="submit" value="Submit">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
@endsection
