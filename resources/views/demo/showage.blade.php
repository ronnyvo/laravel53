@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form method="post" action="{{ url('secret-content') }}">
                    <h3>It seems that you are under 18. Please input your age?</h3>
                    <input type="text" name="age">
                    <input type="submit" value="Go next">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
@endsection
