<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;

class DemoController extends Controller
{
    public function showRequestPage() {
        return view('demo.showrequest');
    }

    public function getRequest(Request $request) {
        if ($request->has('name')) {
            return $request->input('name');
        }
        return 'Nothing';
    }

    public function apiGetUsers() {
        $users = User::all();
        return response()->json($users);
    }

    public function showAgePage() {
        return view('demo.showage');
    }

    public function redirectAction() {
        return redirect('/request');
    }
}
