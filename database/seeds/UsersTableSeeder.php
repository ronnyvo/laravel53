<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Nhan Vo',
            'email' => 'ronny.vo.7@gmail.com',
            'password' => bcrypt('secret'),
        ]);
    }
}
